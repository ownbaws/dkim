require 'spec_helper'
require 'shared_contexts'

describe 'dkim' do
  let(:facts) do
    {
      #:fact_key => "fact_value"
    }
  end

  let(:params) do
    {
      :path => "/etc/dkim",
      :owner => "root",
      :group => "root",
      :selector => "dkim",
      :mode => "0500",
      :purge => false,
      :recurse => false,

    }
  end
  # add these two lines in a single test block to enable puppet and hiera debug mode
  # Puppet::Util::Log.level = :debug
  # Puppet::Util::Log.newdestination(:console)

  it do
    is_expected.to contain_file("dkim")
        .with({
          "ensure"  => "directory",
          "path"    => "/etc/dkim",
          "owner"   => "root",
          "group"   => "root",
          "mode"    => "0500",
          "purge"   => "false",
          "recurse" => "false"
          })
  end
end
