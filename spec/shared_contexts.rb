hiera_config_file = File.expand_path(File.join(File.dirname(__FILE__), 'fixtures','modules','hieradata', 'hiera.yaml'))

shared_context :global_hiera_data do
  let(:hiera_data) do
     {
       "dkim::path" => '/etc/dkim',
       "dkim::group" => 'root',
       "dkim::owner" => 'root',
       "dkim::selector" => 'dkim',
       "dkim::mode" => '0500',
       "dkim::purge" => 'false'
     }
  end
end
