require 'spec_helper'
require 'shared_contexts'

describe 'dkim::domain' do
  let(:title) { 'test_domain' }

  let(:params) do
    {
      :ensure     => 'present',
      :publicKey  => 'publickey',
      :privateKey => 'privatekey',
      :domain     => "test_domain",
      :owner      => "root",
      :group      => "root",
      :selector   => "mail",
      :mode       => "0500"
    }
  end

  let(:pre_condition) { 'class {"::dkim": }' }

  it do
    is_expected.to contain_file("dkim::test_domain.key")
        .with({
          "ensure"  => 'present',
          "path"    => "/etc/dkim/test_domain.mail.key",
          "owner"   => "root",
          "group"   => "root",
          "mode"    => "0500",
          "content" => /privatekey/,
          })
  end

  it do
    is_expected.to contain_file("dkim::test_domain.txt")
        .with({
          "ensure"  => 'present',
          "path"    => "/etc/dkim/test_domain.txt",
          "owner"   => 'root',
          "group"   => 'root',
          "mode"    => "0500",
          "content" => /publickey/,
          })
  end

end
