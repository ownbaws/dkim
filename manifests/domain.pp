# Class=: dkim::domain
define dkim::domain (
  Variant[Boolean, Enum['present', 'absent']] $ensure,
  String $publicKey,
  String $privateKey,
  String $domain = $title,
  String $owner = $dkim::owner,
  String $group = $dkim::group,
  String $selector = $dkim::selector,
  String $mode = '0400'
) {

  file {"dkim::${domain}.key":
    ensure  => $ensure,
    path    => "${dkim::path}/${domain}.${selector}.key",
    owner   => $owner,
    group   => $group,
    mode    => $mode,
    content => template('dkim/private.erb')
  }

  file {"dkim::${domain}.txt":
    ensure  => $ensure,
    path    => "${dkim::path}/${domain}.txt",
    owner   => $owner,
    group   => $group,
    mode    => $mode,
    content => template('dkim/public.erb')
  }
}
