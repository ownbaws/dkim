# Class = dkim
# Your class documentation

class dkim (
  $path,
  $owner,
  $group,
  $selector,
  $mode,
  $purge,
  $recurse,
){
  if ($purge == true and $recurse == false) or ($purge == false and $recurse == true) {
    fail ('Either recurse or purge is set to false, while the other is set to true. Both must have the same value.')
  }

  file {'dkim':
    ensure  => directory,
    path    => $path,
    owner   => $owner,
    group   => $group,
    mode    => $mode,
    purge   => $purge,
    recurse => $recurse
  }
}
