# This is an test file, can be used for reference

class { 'dkim' :
  path    => '/tmp/dkim',
  purge   => true,
  recurse => true
}

dkim::domain { 'example.com':
  ensure     => present,
  publicKey  => 'yourpublickey',
  privateKey => 'yourprivatekey'
}
