dkim
----
# Module Description
Standalone DKIM module for puppet. Useful when using [rmilter](https://rspamd.com/rmilter/) as your DKIM signer.

# Usage
Define the class with the prefered params:
```puppet
class { 'dkim' :
  path  => '/etc/dkim'
}
```

Optional params:
```
path     # Set the location where the dkim files should be added. Default: /etc/dkim
owner    # Owner of whom all files must be owned by. Default: root
group    # Group of whom all files must be owned by. Default: root
selector # Selector used for dkim. Default: dkim
mode     # Directory mode of $path. Default: 0500
purge    # Purge other files in $path. Default: false
```

## Add a domain keys
```puppet
dkim::domain { 'example.com':
  ensure     => present,
  publicKey  => 'yourpublickey',
  privateKey => 'yourprivatekey'
}
```

Required params:
```
enure      # present / absent
publicKey  # Your public key hash in a single line string.
privateKey # Your private key hash in a single string.
```

Optional params:
```
domain   # Default: $tile
owner    # Default: $dkim::owner
group    # Default: $dkim::group
mode     # Default: 0400
selector # Default: $dkim::selector
```
